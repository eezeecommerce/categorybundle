<?php

namespace eezeecommerce\CategoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CategoryPriceFilters
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="eezeecommerce\CategoryBundle\Entity\CategoryPriceFiltersRepository")
 */
class CategoryPriceFilters
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(type="float")
     */
    private $min;

    /**
     * @var integer
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $max;

    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="price_filter")
     * @ORM\JoinTable(name="price_filters")
     */
    private $category;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->category = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set min
     *
     * @param float $min
     *
     * @return CategoryPriceFilters
     */
    public function setMin($min)
    {
        $this->min = $min;

        return $this;
    }

    /**
     * Get min
     *
     * @return float
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Set max
     *
     * @param float $max
     *
     * @return CategoryPriceFilters
     */
    public function setMax($max)
    {
        $this->max = $max;

        return $this;
    }

    /**
     * Get max
     *
     * @return float
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Add category
     *
     * @param \eezeecommerce\CategoryBundle\Entity\Category $category
     *
     * @return CategoryPriceFilters
     */
    public function addCategory(\eezeecommerce\CategoryBundle\Entity\Category $category)
    {
        $this->category[] = $category;

        return $this;
    }

    /**
     * Remove category
     *
     * @param \eezeecommerce\CategoryBundle\Entity\Category $category
     */
    public function removeCategory(\eezeecommerce\CategoryBundle\Entity\Category $category)
    {
        $this->category->removeElement($category);
    }

    /**
     * Get category
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategory()
    {
        return $this->category;
    }
}
