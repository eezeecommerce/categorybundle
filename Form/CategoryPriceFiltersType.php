<?php

namespace eezeecommerce\CategoryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CategoryPriceFiltersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('min')
            ->add('max')
            ->add('category', 'entity', array(
                'class' => 'eezeecommerceCategoryBundle:Category',
                'property' => 'title',
                'multiple' => true,
                "required" => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\CategoryBundle\Entity\CategoryPriceFilters'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_categorybundle_categorypricefilters';
    }
}
