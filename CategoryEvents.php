<?php

namespace eezeecommerce\CategoryBundle;


class CategoryEvents
{
    const CATEGORY_MENU_INITIALISE = "eezeecommerce.category.menu.initialise";

    const CATEGORY_MENU_PRE_CART = "eezeecommerce.category.menu.pre.cart";

    const CATEGORY_MENU_POST_INITIALISE = "eezeecommerce.category.menu.post.initialise";

    const CATEGORY_PAGE_INITIALISE = "eezeecommerce.category.page.initialise";

    const CATEGORY_PAGE_COMPLETED = "eezeecommerce.category.page.completed";
}