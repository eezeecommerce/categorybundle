<?php

namespace eezeecommerce\CategoryBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Translatable\Translatable;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="eezeecommerce\CategoryBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Column(type="boolean")
     */
    protected $disabled = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $show_when_empty = false;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $show_children = true;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="CategoryPriceFilters", mappedBy="category")
     * @ORM\OrderBy({"min" = "ASC"})
     */
    private $price_filter;

    /**
     * @ORM\Column(name="title", type="string", length=64)
     * @Gedmo\Translatable
     */
    private $title;

    /**
     * @ORM\Column(name="short_description", type="string", nullable=true)
     * @Gedmo\Translatable
     */
    private $short_description;

    /**
     * @Gedmo\SortablePosition
     * @ORM\Column(type="integer", nullable=true)
     */
    private $position = 450;

    /**
     * @Gedmo\TreeLeft
     * @ORM\Column(name="lft", type="integer")
     */
    private $lft;

    /**
     * @Gedmo\TreeLevel
     * @ORM\Column(name="lvl", type="integer")
     */
    private $lvl;

    /**
     * @Gedmo\TreeRight
     * @ORM\Column(name="rgt", type="integer")
     */
    private $rgt;

    /**
     * @Gedmo\TreeRoot
     * @ORM\Column(name="root", type="integer")
     */
    private $root;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('1', '2', '3', '4', '6')")
     */
    private $desktop_layout = "4";

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('1', '2', '3', '4', '6')")
     */
    private $tablet_layout = "3";

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('1', '2', '3', '4', '6')")
     */
    private $mobile_layout = "2";

    /**
     * @Gedmo\TreeParent
     * @Gedmo\SortableGroup
     * @ORM\ManyToOne(targetEntity="Category", inversedBy="children", fetch="EAGER")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $parent;

    /**
     * @ORM\OneToMany(targetEntity="Category", mappedBy="parent")
     * @ORM\OrderBy({"position" = "ASC"})
     */
    private $children = null;

    /**
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\ProductBundle\Entity\Product", mappedBy="category")
     * @ORM\OrderBy({"base_price" = "ASC"})
     */
    private $product;

    /**
     * @ORM\ManyToMany(targetEntity="\eezeecommerce\UploadBundle\Entity\Files", cascade={"persist"})
     * @ORM\JoinTable(name="categories_files",
     * joinColumns={@ORM\JoinColumn(name="category_id", referencedColumnName="id")},
     * inverseJoinColumns={@ORM\JoinColumn(name="image_id", referencedColumnName="id")})
     */
    private $image;

    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\UploadBundle\Entity\Files", cascade={"persist"})
     * @ORM\JoinColumn(name="feature_image_id", referencedColumnName="id")
     */
    private $feature_image;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Gedmo\Translatable
     */
    private $feature_text;

    /**
     * @Gedmo\Locale
     */
    protected $locale;

    /**
     * @ORM\OneToOne(targetEntity="\eezeecommerce\WebBundle\Entity\Uri", mappedBy="category", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $slug;
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Set slug
     *
     * @param \eezeecommerce\WebBundle\Entity\Uri $slug
     *
     * @return Category
     */
    public function setSlug(\eezeecommerce\WebBundle\Entity\Uri $slug = null)
    {
        $this->slug = $slug;
        $slug->setCategory($this);
        $slug->setUrl($this->title);

        return $this;
    }
    
    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getParent()
    {
        return $this->parent;
    }

    public function setParent(Category $parent = null)
    {
        $this->parent = $parent;
    }

    /**
     * Get lft
     *
     * @return integer
     */
    public function getLft()
    {
        return $this->lft;
    }

    /**
     * Set lft
     *
     * @param integer $lft
     *
     * @return Category
     */
    public function setLft($lft)
    {
        $this->lft = $lft;

        return $this;
    }

    /**
     * Get lvl
     *
     * @return integer
     */
    public function getLvl()
    {
        return $this->lvl;
    }

    /**
     * Set lvl
     *
     * @param integer $lvl
     *
     * @return Category
     */
    public function setLvl($lvl)
    {
        $this->lvl = $lvl;

        return $this;
    }

    /**
     * Get rgt
     *
     * @return integer
     */
    public function getRgt()
    {
        return $this->rgt;
    }

    /**
     * Set rgt
     *
     * @param integer $rgt
     *
     * @return Category
     */
    public function setRgt($rgt)
    {
        $this->rgt = $rgt;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set root
     *
     * @param integer $root
     *
     * @return Category
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Add child
     *
     * @param \eezeecommerce\CategoryBundle\Entity\Category $child
     *
     * @return Category
     */
    public function addChild(\eezeecommerce\CategoryBundle\Entity\Category $child = null)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \eezeecommerce\CategoryBundle\Entity\Category $child
     */
    public function removeChild(\eezeecommerce\CategoryBundle\Entity\Category $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add product
     *
     * @param \eezeecommerce\ProductBundle\Entity\Product $product
     *
     * @return Category
     */
    public function addProduct(\eezeecommerce\ProductBundle\Entity\Product $product)
    {
        $this->product[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param \eezeecommerce\ProductBundle\Entity\Product $product
     */
    public function removeProduct(\eezeecommerce\ProductBundle\Entity\Product $product)
    {
        $this->product->removeElement($product);
    }

    /**
     * Get product
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Add image
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $image
     *
     * @return Category
     */
    public function addImage(\eezeecommerce\UploadBundle\Entity\Files $image)
    {
        $this->image[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $image
     */
    public function removeImage(\eezeecommerce\UploadBundle\Entity\Files $image)
    {
        $this->image->removeElement($image);
    }

    /**
     * Get image
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Category
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     *
     * @return Category
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get showWhenEmpty
     *
     * @return boolean
     */
    public function getShowWhenEmpty()
    {
        return $this->show_when_empty;
    }

    /**
     * Set showWhenEmpty
     *
     * @param boolean $showWhenEmpty
     *
     * @return Category
     */
    public function setShowWhenEmpty($showWhenEmpty)
    {
        $this->show_when_empty = $showWhenEmpty;

        return $this;
    }

    /**
     * Get showChildren
     *
     * @return boolean
     */
    public function getShowChildren()
    {
        if (null === $this->show_children) {
            $this->setShowChildren(true);
        }

        return $this->show_children;
    }

    /**
     * Set showChildren
     *
     * @param boolean $showChildren
     *
     * @return Category
     */
    public function setShowChildren($showChildren)
    {
        $this->show_children = $showChildren;

        return $this;
    }

    /**
     * Get desktopLayout
     *
     * @return string
     */
    public function getDesktopLayout()
    {
        return $this->desktop_layout;
    }

    /**
     * Set desktopLayout
     *
     * @param string $desktopLayout
     *
     * @return Category
     */
    public function setDesktopLayout($desktopLayout)
    {
        $this->desktop_layout = $desktopLayout;

        return $this;
    }

    /**
     * Get tableLayout
     *
     * @return string
     */
    public function getTabletLayout()
    {
        return $this->tablet_layout;
    }

    /**
     * Set tableLayout
     *
     * @param string $tableLayout
     *
     * @return Category
     */
    public function setTabletLayout($tableLayout)
    {
        $this->tablet_layout = $tableLayout;

        return $this;
    }

    /**
     * Get mobileLayout
     *
     * @return string
     */
    public function getMobileLayout()
    {
        return $this->mobile_layout;
    }

    /**
     * Set mobileLayout
     *
     * @param string $mobileLayout
     *
     * @return Category
     */
    public function setMobileLayout($mobileLayout)
    {
        $this->mobile_layout = $mobileLayout;

        return $this;
    }

    /**
     * Get featureText
     *
     * @return string
     */
    public function getFeatureText()
    {
        return $this->feature_text;
    }

    /**
     * Set featureText
     *
     * @param string $featureText
     *
     * @return Category
     */
    public function setFeatureText($featureText)
    {
        $this->feature_text = $featureText;

        return $this;
    }

    /**
     * Get featureImage
     *
     * @return \eezeecommerce\UploadBundle\Entity\Files
     */
    public function getFeatureImage()
    {
        return $this->feature_image;
    }

    /**
     * Set featureImage
     *
     * @param \eezeecommerce\UploadBundle\Entity\Files $featureImage
     *
     * @return Category
     */
    public function setFeatureImage(\eezeecommerce\UploadBundle\Entity\Files $featureImage = null)
    {
        $this->feature_image = $featureImage;

        return $this;
    }

    /**
     * Set shortDescription
     *
     * @param string $shortDescription
     *
     * @return Category
     */
    public function setShortDescription($shortDescription)
    {
        $this->short_description = $shortDescription;

        return $this;
    }

    /**
     * Get shortDescription
     *
     * @return string
     */
    public function getShortDescription()
    {
        return $this->short_description;
    }

    /**
     * Add priceFilter
     *
     * @param \eezeecommerce\CategoryBundle\Entity\CategoryPriceFilters $priceFilter
     *
     * @return Category
     */
    public function addPriceFilter(\eezeecommerce\CategoryBundle\Entity\CategoryPriceFilters $priceFilter)
    {
        $this->price_filter[] = $priceFilter;

        return $this;
    }

    /**
     * Remove priceFilter
     *
     * @param \eezeecommerce\CategoryBundle\Entity\CategoryPriceFilters $priceFilter
     */
    public function removePriceFilter(\eezeecommerce\CategoryBundle\Entity\CategoryPriceFilters $priceFilter)
    {
        $this->price_filter->removeElement($priceFilter);
    }

    /**
     * Get priceFilter
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPriceFilter()
    {
        return $this->price_filter;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return Product
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }
}
