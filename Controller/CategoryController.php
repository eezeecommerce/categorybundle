<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace eezeecommerce\CategoryBundle\Controller;

use eezeecommerce\CategoryBundle\CategoryEvents;
use eezeecommerce\CategoryBundle\Entity\Category;
use eezeecommerce\CategoryBundle\Event\CategoryPageEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use eezeecommerce\WebBundle\Entity\Uri;

/**
 * Description of CategoryController
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class CategoryController extends Controller
{
    public function indexAction(Request $request)
    {
        $dispatcher = $this->get("event_dispatcher");

        $params = array();

        $event = new CategoryPageEvent($params);

        $dispatcher->dispatch(CategoryEvents::CATEGORY_PAGE_INITIALISE, $event);

        $route = $request->attributes->get("_route");
        
        $em = $this->get('doctrine.orm.entity_manager');

        if ($request->query->has("min-price") && $request->query->has("max-price")) {
            if ($request->query->get("max-price") == 0.00) {
                $dql = "SELECT p FROM eezeecommerceProductBundle:Product p
                JOIN p.category as c
                JOIN c.slug as u
                WHERE u.uri_key = :route
                AND p.disabled = 0
                AND p.base_price > :min
                ORDER BY p.base_price";
                $query = $em->createQuery($dql);
                $query->setParameter('min', $request->query->get("min-price"));
            }else {
                $dql = "SELECT p FROM eezeecommerceProductBundle:Product p
                JOIN p.category as c
                JOIN c.slug as u
                WHERE u.uri_key = :route
                AND p.disabled = 0
                AND p.base_price BETWEEN :min AND :max
                ORDER BY p.base_price";
                $query = $em->createQuery($dql);
                $query->setParameter('min', $request->query->get("min-price"));
                $query->setParameter('max', $request->query->get("max-price"));
            }
        } else {
        $dql = "SELECT p FROM eezeecommerceProductBundle:Product p
                JOIN p.category as c
                JOIN c.slug as u
                WHERE u.uri_key = :route
                AND p.disabled = 0
                ORDER BY p.base_price";
            $query = $em->createQuery($dql);
        }
        $query->setParameter('route', $route);
        
        $paginator = $this->get('knp_paginator');

        $limit = 20;

        $page = $request->query->getInt('page', 1);

        if ($request->query->has("viewall")) {
            $test = clone $query;
            if ($request->query->has("min-price") && $request->query->has("max-price")) {
                if ($request->query->get("max-price") == 0.00) {
                    $test->setParameter('min', $request->query->get("min-price"));
                } else {
                    $test->setParameter('min', $request->query->get("min-price"));
                    $test->setParameter('max', $request->query->get("max-price"));
                }
            }

            $test->setParameter('route', $route);
            $limit = count($test->getResult());
            $page = 1;
        }

        if ($limit <= 0) {
            $limit = 20;
        }

        $pagination = $paginator->paginate(
            $query,
            $page,
            $limit,
            array("defaultSortFieldName" => "p.base_price", 'defaultSortDirection' => "ASC")
        );


        
        $uri = $this->getDoctrine()
                ->getRepository("eezeecommerceWebBundle:Uri")
                ->findOneBy(array("uri_key" => $route));
        
        $currentCategory = $uri->getCategory();

        $filters = $currentCategory->getPriceFilter();
        
        $categories = $currentCategory->getChildren();

        $manager = $this->get("eezeecommerce.pricing.manager");

        foreach ($categories as $k => $category) {
            $products = $category->getProduct();
            foreach ($products as $key => $product)
            {
                $category->removeProduct($product);
                $pricing = $manager->getProductPrice($product);
                $category->addProduct($pricing);
                break;
            }

            $categories[$k] = $category;
        }

        foreach ($pagination as $key => $item)
        {
            $product = $manager->getProductPrice($item);
            $pagination[$key] = $product;
        }

        $params = $event->getParams();

        $params["pagination"] = $pagination;

        $params["categories"] = $categories;

        $params["uri"] = $uri;

        $params["filters"] = $filters;

        $event->setParams($params);

        $dispatcher->dispatch(CategoryEvents::CATEGORY_PAGE_COMPLETED, $event);

        return $this->render("AppBundle:Category:index.html.twig", $event->getParams());
    }
}
