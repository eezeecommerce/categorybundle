<?php

namespace eezeecommerce\CategoryBundle\Event;

use Symfony\Component\EventDispatcher\Event;

/**
 * Class MenuEvent
 * @package eezeecommerce\CategoryBundle\Event
 */
class MenuEvent extends Event
{
    /**
     * Menu Array
     * @var array
     */
    private $menu;

    /**
     * MenuEvent constructor.
     * @param array $menu Menu array
     */
    public function __construct(array $menu)
    {
        $this->menu = $menu;
    }

    /**
     * Return menu array
     * @return array
     */
    public function getMenu()
    {
        return $this->menu;
    }

    /**
     * Set menu array
     *
     * @param array $menu Menu array
     *
     * @return void
     */
    public function setMenu(array $menu)
    {
        $this->menu = $menu;
    }
}