<?php

namespace eezeecommerce\CategoryBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use eezeecommerce\WebBundle\Form\UriType;
use eezeecommerce\UploadBundle\Form\FilesType;

class CategoryType extends AbstractType
{

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', null, ["label" => false])
            ->add('short_description', null, ["label" => false])
            ->add('slug', new UriType())
            ->add(
                'image',
                "collection",
                array(
                    "label" => false,
                    'type' => new FilesType(),
                    'required' => false,
                    'allow_add' => true,
                    'allow_delete' => true,
                    'prototype' => true,
                    "by_reference" => false,
                    'prototype_name' => "__category_image__",
                )
            )
            ->add(
                'feature_image',
                FilesType::class,
                array(
                    "label" => false,
                    "required" => false,
                )
            )
            ->add(
                'feature_text',
                null,
                [
                "label" => false,
                    "required" => false,
                ]
            )
            ->add('disabled', null, ["label" => "Disable Category"])
            ->add('show_when_empty', null, ["label" => "Show category when empty"])
            ->add('show_children', null, ["label" => "Show category children in menu"])
            ->add(
                'desktop_layout',
                'choice',
                array(
                    "label" => false,
                    'choices' => array(
                        1 => '1 Product/Category per row',
                        2 => '2 Products/Categories per row',
                        3 => '3 Products/Categories per row',
                        4 => '4 Products/Categories per row',
                        6 => '6 Products/Categories per row',
                    ),
                    "preferred_choices" => function ($val, $key) {
                        return $val == '4 Products/Categories per row';
                    },
                )
            )
            ->add(
                'tablet_layout',
                'choice',
                array(
                    "label" => false,
                    'choices' => array(
                        1 => '1 Product/Category per row',
                        2 => '2 Products/Categories per row',
                        3 => '3 Products/Categories per row',
                        4 => '4 Products/Categories per row',
                        6 => '6 Products/Categories per row',
                    ),
                    "preferred_choices" => function ($val, $key) {
                        return $val == '3 Products/Categories per row';
                    },
                )
            )
            ->add(
                'mobile_layout',
                ChoiceType::class,
                array(
                    "label" => false,
                    'choices' => array(
                        1 => '1 Product/Category per row',
                        2 => '2 Products/Categories per row',
                        3 => '3 Products/Categories per row',
                        4 => '4 Products/Categories per row',
                        6 => '6 Products/Categories per row',
                    ),
                    "preferred_choices" => function ($val, $key) {
                        return $val == '2 Products/Categories per row';
                    },
                )
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'eezeecommerce\CategoryBundle\Entity\Category',
            )
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_categorybundle_category';
    }

}
