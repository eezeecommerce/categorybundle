<?php

namespace eezeecommerce\CategoryBundle\Menu;

use eezeecommerce\CartBundle\Core\CartManager;
use eezeecommerce\CategoryBundle\CategoryEvents;
use eezeecommerce\CategoryBundle\Event\MenuEvent;
use eezeecommerce\SettingsBundle\Entity\Settings;
use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Description of Menu
 *
 * @author Liam Sorsby <liam@eezeecommerce.com>
 * @author Daniel Sharp <dan@eezeecommerce.com>
 */
class Menu extends Controller
{

    protected $container;

    protected $template;

    /**
     * @var Settings
     */
    private $settings;

    /**
     * @var CartManager
     */
    protected $manager;

    /**
     * @var EventDispatcher
     */
    protected $dispatcher;

    protected $cart_enabled;

    protected $show_empty;

    /**
     * Menu constructor.
     * @param Container $container
     * @param CartManager $manager
     */
    public function __construct(Container $container)
    {
        $this->container = $container;
        $this->template = "eezeecommerceCategoryBundle::menu.html.twig";
        $this->setSettings();
    }

    /**
     * @param SettingsProvider $settings
     */
    public function setSettings()
    {
        $settings = $this->getDoctrine()->getRepository("eezeecommerceSettingsBundle:Settings")->getMenuSettings();
        $this->cart_enabled = $settings["cart_enabled"];
        $this->show_empty = $settings["category_show_empty"];
    }



    /**
     * @param EventDispatcher $dispatcher
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function createMainMenu()
    {
        $categories = $this->getDoctrine()->getRepository("eezeecommerceCategoryBundle:Category")->findBy(["disabled" => false], ["position" => "ASC"]);

        $event = new MenuEvent([]);

        $this->dispatcher->dispatch(CategoryEvents::CATEGORY_MENU_INITIALISE, $event);

        $menu = $event->getMenu();

        foreach ($categories as $category) {
            if (0 !== $category->getLvl()) {
                continue;
            }
            $menu[$category->getTitle()] = [
                "title" => $category->getTitle(),
                'route' => $category->getSlug()->getUriKey(),
                'position' => $category->getPosition(),
            ];

            if (count($category->getChildren()) > 0 && $category->getShowChildren()) {
                $check = false;
                foreach ($category->getChildren() as $children) {
                    if (1 !== $children->getLvl()) {
                        continue;
                    }

                    if (true === $children->getDisabled()) {
                        continue;
                    }

                    $menu[$children->getParent()->getTitle()]["child"][$children->getTitle()] = [
                        "title" => $children->getTitle(),
                        'route' => $children->getSlug()->getUriKey(),
                        'position' => $children->getPosition(),
                    ];
                    if (null !== $children->getChildren() && $category->getShowChildren()) {
                        $check2 = false;
                        foreach ($children->getChildren() as $subChild) {
                            if (2 !== $subChild->getLvl()) {
                                continue;
                            }

                            if (true === $subChild->getDisabled()) {
                                continue;
                            }

                            $menu[$children->getParent()->getTitle()]["child"][$subChild->getParent()->getTitle()]["child"][$subChild->getTitle()] = [
                                'title' => $subChild->getTitle(),
                                'route' => $subChild->getSlug()->getUriKey(),
                                'position' => $subChild->getPosition(),
                            ];

                            foreach ($subChild->getChildren() as $subSubChild) {
                                $menu[$children->getParent()->getTitle()]["child"][$subChild->getParent()->getTitle()]["child"][$subSubChild->getParent()->getTitle()]["child"][$subSubChild->getTitle()] = [
                                    'title' => $subSubChild->getTitle(),
                                    'route' => "homepage"
                                ];
                            }
                        }
                    }
                }
            }
        }

        $event->setMenu($menu);

        $this->dispatcher->dispatch(CategoryEvents::CATEGORY_MENU_PRE_CART, $event);

        $menu = $event->getMenu();

        if ($this->cart_enabled) {
            $name = $this->get('translator')->trans('My Cart');

            $menu["cart"] = [
                "title" => $name,
                "route" => "_eezeecommerce_cart",
                "attributes" => [
                    "class" => "dropdown dropdown-mega",
                ],
            ];
        }


        $event->setMenu($menu);

        $this->dispatcher->dispatch(CategoryEvents::CATEGORY_MENU_POST_INITIALISE, $event);

        $menu = $event->getMenu();

        return $this->renderView($this->template, ["main" => $menu]);
    }
}
